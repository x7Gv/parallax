#ifndef TINYOBJ_HELPER_H_
#define TINYOBJ_HELPER_H_

#include <stdlib.h>
#include <stdio.h>

#include <float.h>
#include <limits.h>
#include <math.h>
#include <string.h>
#include <assert.h>

#ifdef _WIN64
#define atoll(S) _atoi64(S)
#include <windows.h>
#else
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <unistd.h>
#endif

static const char* mmap_file(size_t* len, const char* filename) 
{
        
#ifdef _WIN64
        HANDLE file =
        CreateFileA(filename, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
              FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);
        assert(file != INVALID_HANDLE_VALUE);

        HANDLE fileMapping = CreateFileMapping(file, NULL, PAGE_READONLY, 0, 0, NULL);
        assert(fileMapping != INVALID_HANDLE_VALUE);

        LPVOID fileMapView = MapViewOfFile(fileMapping, FILE_MAP_READ, 0, 0, 0);
        auto fileMapViewChar = (const char*)fileMapView;
        assert(fileMapView != NULL);
#else

        FILE* f;
        long file_size;
        struct stat sb;
        char* p;
        int fd;

        (*len) = 0;

        f = fopen(filename, "r");
        fseek(f, 0, SEEK_END);
        file_size = ftell(f);
        fclose(f);

        fd = open(filename, O_RDONLY);
        if (fd == -1) {
            perror("open");
            return NULL;
    }

    if (fstat(fd, &sb) == -1) {
            perror("fstat");
            return NULL;
    }

    if (!S_ISREG(sb.st_mode)) {
            fprintf(stderr, "%s is not a file\n", "lineitem.tbl");
            return NULL;
    }

    p = (char*)mmap(0, (size_t)file_size, PROT_READ, MAP_SHARED, fd, 0);

    if (p == MAP_FAILED) {
            perror("mmap");
            return NULL;
    }

    if (close(fd) == -1) {
            perror("close");
            return NULL;
    }

    (*len) = (size_t)file_size;

    return p;

#endif
}

static const char *get_file_data(size_t *len, const char *filename) 
{
        const char *ext = strrchr(filename, '.');

        size_t data_len = 0;
        const char *data = NULL;

        if (strcmp(ext, ".gz") == 0) {
                assert(0); /* todo */

        } else {
                 data = mmap_file(&data_len, filename);
        }

         (*len) = data_len;
         return data;
}

#endif /* !TINYOBJ_HELPER_H */